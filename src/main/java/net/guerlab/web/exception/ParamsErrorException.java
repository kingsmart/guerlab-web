package net.guerlab.web.exception;

import org.apache.commons.lang3.StringUtils;

import net.guerlab.commons.exception.ApplicationException;

/**
 * 参数异常
 *
 * @author guer
 *
 */
public class ParamsErrorException extends ApplicationException {

    private static final long serialVersionUID = 1L;

    private static int errorCode = 700;

    static {
        String value = System.getProperty("net.guerlab.errorCode.ParamsErrorException");

        if (!StringUtils.isBlank(value)) {
            int errorCode = 0;
            try {
                errorCode = Integer.parseInt(value);
            } catch (Exception e) {
                /*
                 * don't need a log
                 */
            }

            if (errorCode > 0) {
                ParamsErrorException.errorCode = errorCode;
            }
        }
    }

    /**
     * 创建并初始化参数异常
     *
     * @param message
     *            参数异常信息
     * @param cause
     *            错误栈信息
     */
    public ParamsErrorException(String message, Throwable cause) {
        super(message, cause, errorCode);
    }

    /**
     * 创建并初始化参数异常
     *
     * @param message
     *            参数异常信息
     */
    public ParamsErrorException(String message) {
        super(message, errorCode);
    }

}
