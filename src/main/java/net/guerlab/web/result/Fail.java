package net.guerlab.web.result;

/**
 * 通用失败JSON返回结果集
 *
 * @author guer
 *
 * @param <T>
 *            数据类型
 */
public class Fail<T> extends Result<T> {

    private static final String MSG = "fail";

    /**
     * 无参构造
     */
    public Fail() {
        this(MSG, null);
    }

    /**
     * 通过设置消息内容来初始化结果集
     *
     * @param message
     *            消息内容
     */
    public Fail(String message) {
        this(message, null);
    }

    /**
     * 通过设置数据来初始化结果集
     *
     * @param data
     *            数据
     */
    public Fail(T data) {
        this(MSG, data);
    }

    /**
     * 通过设置消息内容和数据来初始化结果集
     *
     * @param message
     *            消息内容
     * @param data
     *            数据
     */
    public Fail(String message, T data) {
        super(false, message, data);
    }

    /**
     * 通过设置数据来初始化结果集
     *
     * @param message
     *            消息内容
     * @param errorCode
     *            错误代码
     */
    public Fail(String message, int errorCode) {
        this(message, null);
        this.errorCode = errorCode;
    }
}