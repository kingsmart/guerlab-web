package net.guerlab.web.result;

/**
 * 通用成功JSON返回结果集
 *
 * @author guer
 *
 * @param <T>
 *            数据类型
 */
public class Succeed<T> extends Result<T> {

    /**
     * 默认消息
     */
    public static final String MSG = "success";

    /**
     * 无参构造
     */
    public Succeed() {
        this(MSG, null);
    }

    /**
     * 通过设置消息内容来初始化结果集
     *
     * @param message
     *            消息内容
     */
    public Succeed(String message) {
        this(message, null);
    }

    /**
     * 通过设置数据来初始化结果集
     *
     * @param data
     *            数据
     */
    public Succeed(T data) {
        this(MSG, data);
    }

    /**
     * 通过设置消息内容和数据来初始化结果集
     *
     * @param message
     *            消息内容
     * @param data
     *            数据
     */
    public Succeed(String message, T data) {
        super(true, message, data);
    }
}